program MoveFiles;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils, System.Classes, System.IOUtils;

var
  Origem: string;
  Destino: string;
  ListaArquivos: TstringList;
  I: Integer;

begin
  try
    if ParamCount <> 3 then
    begin
      Writeln('MoveFiles 1.0.0 By Marcio Martins - 2014');
      Writeln('Parametros inv�lidos.');
      Writeln('Modo de usar:');
      Writeln('MoveFiles.exe Origem Destino ListaArquivo.txt');
      Writeln('MoveFiles.exe C:\Origem C:\Destino ListaArquivo.txt');
    end
    else
    begin
      ListaArquivos := TStringList.Create;
      try
        Origem := IncludeTrailingPathDelimiter(ParamStr(1));
        Destino := IncludeTrailingPathDelimiter(ParamStr(2));
        ListaArquivos.LoadFromFile(ParamStr(3));
        if ListaArquivos.Count > 0 then
        begin
          ForceDirectories(Destino);
          for I := 0 to ListaArquivos.Count -1 do
            if FileExists(Origem+ListaArquivos.Strings[I]) then
              TFile.Move(Origem+ListaArquivos.Strings[I], Destino+ListaArquivos.Strings[I]);
        end;
        Writeln('Conclu�do.');
      finally
        ListaArquivos.Free;
      end;
    end;

  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
